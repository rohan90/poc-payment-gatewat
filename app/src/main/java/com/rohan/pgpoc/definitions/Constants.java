package com.rohan.pgpoc.definitions;

/**
 * Created by rohan on 11/3/16.
 */
public class Constants {

    public static final String PROD_URL = "http://:8090/api/v1/";
    public static final String LOCAL_URL = "http://192.168.1.2:8090/api/v1/";
    public static final String DEV_URL = "http://hackerearth.0x10.info";

    private static final String API_VERSION = "/api";

    //ACTUAL URL
    public static final String BASE_URL = DEV_URL + API_VERSION;

    public static final int CONNECTION_TIMEOUT = 10;

    //sharedPrefs file
    public static final String PREFERENCES_FILENAME = "pgPoC_PREFERENCES_FILE";

    public static final String LINKED_IN = "https://in.linkedin.com/in/rohan90";

    public class BUNDLE_KEYS {
        public static final String PAYMENT_GATEWAY = "paymentGateway";
        public static final String WEBVIEW_URL = "url";
    }

    public class REQUEST_CODES {
    }

    public class RESULT_CODES {
    }

    public class REST_API {
        public static final String PAYMENT_PORTAL = "/payment_portals?type=json&query=";
        public static final String GET_LIST_OF_GATEWAYS = PAYMENT_PORTAL + "list_gateway";
        public static final String GET_API_HITS = PAYMENT_PORTAL +"api_hits";
    }

}
