package com.rohan.pgpoc.utils;

import android.app.Activity;
import android.view.View;

import com.norbsoft.typefacehelper.TypefaceHelper;

/**
 * Created by rohan on 11/3/16.
 */
public class FontUtils {
    public static void init(Activity activity) {
        TypefaceHelper.typeface(activity);
    }

    public static void init(View view) {
        TypefaceHelper.typeface(view);
    }
}
