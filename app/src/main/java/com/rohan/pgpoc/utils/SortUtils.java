package com.rohan.pgpoc.utils;

import com.rohan.pgpoc.models.PaymentGateway;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by rohan on 12/3/16.
 */
public class SortUtils {

    public static List<PaymentGateway> sortByCost(List<PaymentGateway> sortedList) {
        Collections.sort(sortedList, new Comparator<PaymentGateway>() {
            @Override
            public int compare(PaymentGateway lhs, PaymentGateway rhs) {
                int lhsCost = Integer.parseInt(lhs.getSetupCost());
                int rhsCost = Integer.parseInt(rhs.getSetupCost());
                return lhsCost<rhsCost?-1:lhsCost>rhsCost?1:0;
            }
        });
        return sortedList;
    }

    public static List<PaymentGateway> sortByRating(List<PaymentGateway> sortedList) {
        Collections.sort(sortedList, new Comparator<PaymentGateway>() {
            @Override
            public int compare(PaymentGateway lhs, PaymentGateway rhs) {
                float lhsRating = Float.parseFloat(lhs.getRating());
                float rhsRating = Float.parseFloat(rhs.getRating());
                return lhsRating<rhsRating?-1:lhsRating>rhsRating?1:0;
            }
        });
        return sortedList;
    }

    public static List<PaymentGateway> sortByName(List<PaymentGateway> sortedList) {
        Collections.sort(sortedList, new Comparator<PaymentGateway>() {
            @Override
            public int compare(PaymentGateway lhs, PaymentGateway rhs) {
                String lhsName = lhs.getName();
                String rhsName = rhs.getName();
                return lhsName.compareTo(rhsName);
            }
        });
        return sortedList;
    }
}
