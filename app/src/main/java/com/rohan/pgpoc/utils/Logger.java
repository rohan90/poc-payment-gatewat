package com.rohan.pgpoc.utils;

import android.util.Log;

import com.rohan.pgpoc.ui.utils.MiscUtils;

/**
 * Created by rohan on 12/3/16.
 */
public class Logger {
    private static final boolean isLoggerOn = true;
    private static final String TAG = "pgPOClogs";

    public static void logError(String message) {
        if (MiscUtils.isLoggerOn())
            Log.e(TAG, message);
    }

    public static void logInfo(String message) {
        if (MiscUtils.isLoggerOn())
            Log.d(TAG, message);
    }
}
