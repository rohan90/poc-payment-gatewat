package com.rohan.pgpoc.models.responseDTOs;

import com.google.gson.annotations.SerializedName;
import com.rohan.pgpoc.models.PaymentGateway;

import java.util.List;

/**
 * Created by rohan on 12/3/16.
 */
public class GetPaymentGatewayListResponseDTO {
    @SerializedName("payment_gateways")
    private List<PaymentGateway> data;

    public List<PaymentGateway> getData() {
        return data;
    }
}
