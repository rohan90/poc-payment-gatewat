package com.rohan.pgpoc.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by rohan on 11/3/16.
 */
public class PaymentGateway implements Parcelable{
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private String image;

    @SerializedName("description")
    private String description;

    @SerializedName("branding")
    private String branding;

    @SerializedName("rating")
    private String rating;

    @SerializedName("currencies")
    private String currencies;

    @SerializedName("setup_fee")
    private String setupCost;

    @SerializedName("transaction_fees")
    private String transactionCost;

    @SerializedName("how_to_document")
    private String instructions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBranding() {
        return branding;
    }

    public void setBranding(String branding) {
        this.branding = branding;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCurrencies() {
        return currencies;
    }

    public void setCurrencies(String currencies) {
        this.currencies = currencies;
    }

    public String getSetupCost() {
        return setupCost;
    }

    public void setSetupCost(String setupCost) {
        this.setupCost = setupCost;
    }

    public String getTransactionCost() {
        return transactionCost;
    }

    public void setTransactionCost(String transactionCost) {
        this.transactionCost = transactionCost;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "PaymentGateway{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                ", branding='" + branding + '\'' +
                ", rating='" + rating + '\'' +
                ", currencies='" + currencies + '\'' +
                ", setupCost='" + setupCost + '\'' +
                ", transactionCost='" + transactionCost + '\'' +
                ", instructions='" + instructions + '\'' +
                '}';
    }

    /**
     * Parcelable
     */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.image);
        dest.writeString(this.description);
        dest.writeString(this.branding);
        dest.writeString(this.rating);
        dest.writeString(this.currencies);
        dest.writeString(this.setupCost);
        dest.writeString(this.transactionCost);
        dest.writeString(this.instructions);
    }

    public PaymentGateway() {
    }

    protected PaymentGateway(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.image = in.readString();
        this.description = in.readString();
        this.branding = in.readString();
        this.rating = in.readString();
        this.currencies = in.readString();
        this.setupCost = in.readString();
        this.transactionCost = in.readString();
        this.instructions = in.readString();
    }

    public static final Creator<PaymentGateway> CREATOR = new Creator<PaymentGateway>() {
        public PaymentGateway createFromParcel(Parcel source) {
            return new PaymentGateway(source);
        }

        public PaymentGateway[] newArray(int size) {
            return new PaymentGateway[size];
        }
    };
}
