package com.rohan.pgpoc.models.responseDTOs;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rohan on 12/3/16.
 */
public class GetApiHitsResponseDTO {
    @SerializedName("api_hits")
    private String data;

    public String getData() {
        return data;
    }
}
