package com.rohan.pgpoc.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rohan.pgpoc.R;

/**
 * Created by rohan on 11/3/16.
 */
public class BlankFragment extends Fragment{
    private static BlankFragment instance;
    private View view;
    public BlankFragment() {
    }

    public static BlankFragment getInstance() {
        if (instance == null)
            instance = new BlankFragment();
        else instance.clearData();

        return instance;
    }

    private void clearData() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_blank, container, false);

        return view;
    }
}
