package com.rohan.pgpoc.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.rohan.pgpoc.R;
import com.rohan.pgpoc.definitions.Constants;
import com.rohan.pgpoc.models.PaymentGateway;
import com.rohan.pgpoc.models.dao.PaymentGatewayDao;
import com.rohan.pgpoc.ui.utils.MiscUtils;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by rohan on 12/3/16.
 */
public class PaymentGatewayPreviewActivity extends BaseActivity {

    private PaymentGateway paymentGateway;
    private TextView tvTitle;
    private RatingBar rbRating;
    private TextView tvDescription;
    private TextView tvFee;
    private TextView tvBranding;
    private TextView tvCurrencies;
    private CircleImageView civPic;
    private LinearLayout containerForShare;
    private LinearLayout containerForLink;
    private LinearLayout containerForLike;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_gateway_preview);

        init();
    }

    private void init() {
        initData();
        initViews();
        initContainers();
    }

    private void initData() {
        paymentGateway = (PaymentGateway) getIntent().getParcelableExtra(Constants.BUNDLE_KEYS.PAYMENT_GATEWAY);
    }

    private void initViews() {
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvBranding = (TextView) findViewById(R.id.tv_branding);
        tvFee = (TextView) findViewById(R.id.tv_transaction_fee);
        tvCurrencies = (TextView) findViewById(R.id.tv_currencies);
        tvDescription = (TextView) findViewById(R.id.tv_description);
        rbRating = (RatingBar) findViewById(R.id.rb_gateway);
        civPic = (CircleImageView) findViewById(R.id.civ_pic);

        tvTitle.setText(paymentGateway.getName());
        tvBranding.setText(paymentGateway.getBranding());
        tvFee.setText(paymentGateway.getTransactionCost());
        tvCurrencies.setText(paymentGateway.getCurrencies());
        tvDescription.setText(paymentGateway.getDescription());
        rbRating.setRating(Float.parseFloat(paymentGateway.getRating()));
        Picasso.with(this).load(paymentGateway.getImage()).placeholder(R.drawable.ic_launcher).into(civPic);
    }

    private void initContainers() {
        containerForShare = (LinearLayout) findViewById(R.id.ll_container_for_share);
        containerForLink = (LinearLayout) findViewById(R.id.ll_container_for_link);
        containerForLike = (LinearLayout) findViewById(R.id.ll_container_for_like);

        containerForLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWebView(paymentGateway.getInstructions());
            }
        });

        containerForShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareLink();
            }
        });

        containerForLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    saveInDB(paymentGateway);
                }catch (Exception e){
                    // do not know realm that well so duplicate key exceptions..
                    // ....RealmPrimaryKeyConstraintException, but meh.
                }
            }
        });
    }

    private void showWebView(String url) {
        Intent intent = new Intent(this,EmbeddedWebViewActivity.class);
        intent.putExtra(Constants.BUNDLE_KEYS.WEBVIEW_URL, url);
        startActivity(intent);
    }

    private void shareLink() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Check out this payment gatway awesome!! " + paymentGateway.getInstructions());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void saveInDB(PaymentGateway paymentGateway) {
        Realm realm = Realm.getInstance(this);
        realm.beginTransaction();
        RealmObject gateway = realm.copyToRealm(getDaoFromPojo(paymentGateway));
        realm.commitTransaction();
    }

    private RealmObject getDaoFromPojo(PaymentGateway gateway) {
        PaymentGatewayDao dao = new PaymentGatewayDao();
        dao.setId(gateway.getId());
        dao.setName(gateway.getName());
        dao.setBranding(gateway.getBranding());
        dao.setCurrencies(gateway.getCurrencies());
        dao.setDescription(gateway.getDescription());
        dao.setImage(gateway.getImage());
        dao.setInstructions(gateway.getInstructions());
        dao.setRating(gateway.getRating());
        dao.setSetupCost(gateway.getSetupCost());
        dao.setTransactionCost(gateway.getTransactionCost());
        return dao;
    }
}
