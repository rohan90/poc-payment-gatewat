package com.rohan.pgpoc.ui.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rohan.pgpoc.R;
import com.rohan.pgpoc.definitions.Constants;

/**
 * Created by rohan on 12/3/16.
 */
public class SelfPromotionFragment extends Fragment{
    private static SelfPromotionFragment instance;
    private View view;
    public SelfPromotionFragment() {
    }

    public static SelfPromotionFragment getInstance() {
        if (instance == null)
            instance = new SelfPromotionFragment();
        else instance.clearData();

        return instance;
    }

    private void clearData() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_self_promotion, container, false);

        initViews();
        return view;
    }

    private void initViews() {
        TextView tvLinkedIn = (TextView) view.findViewById(R.id.tv_link);
        tvLinkedIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLinkedInBrowser();
            }
        });
    }

    private void openLinkedInBrowser() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.LINKED_IN));
        startActivity(browserIntent);
    }
}
