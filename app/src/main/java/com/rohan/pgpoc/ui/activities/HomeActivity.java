package com.rohan.pgpoc.ui.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.rohan.pgpoc.R;
import com.rohan.pgpoc.ui.fragments.BlankFragment;
import com.rohan.pgpoc.ui.fragments.HomeFragment;
import com.rohan.pgpoc.ui.fragments.LikedFragment;
import com.rohan.pgpoc.ui.fragments.SelfPromotionFragment;
import com.rohan.pgpoc.utils.FontUtils;

/**
 * Created by rohan on 11/3/16.
 */
public class HomeActivity extends BaseActivity {

    private Toolbar mToolbar;
    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        FontUtils.init(this);
        initViews();
    }

    private void initViews() {
        initToolbar();
        initNavigationDrawer();
        setFirstItemNavigationView();
    }

    private void setFirstItemNavigationView() {
        mNavigationView.setCheckedItem(R.id.home);
        mNavigationView.getMenu().performIdentifierAction(R.id.home, 0);
        mNavigationView.getMenu().getItem(0).setChecked(true);
        setTitle(R.string.nav_home);
    }

    private void replaceFragment(Fragment fragment) {
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }

    private void setToolBarTitle(int id) {
        mToolbar.setTitle(id);
    }

    private void closeApplication() {
        finish();
    }

    /*on back pressed twice exits app*/
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed(); // or i could call finish
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        toast("Please press BACK again to exit.");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    /**
     * init Methods
     */

    private void initToolbar() {
        // Set a toolbar to replace the action bar.
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    private void initNavigationDrawer() {
        //Initializing NavigationView
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        mNavigationView.setNavigationItemSelectedListener(new MyNavigationView());

        // Initializing Drawer Layout and ActionBarToggle
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    /**
     * NavigationView
     */
    private class MyNavigationView implements NavigationView.OnNavigationItemSelectedListener {

        // This method will trigger on item Click of navigation menu
        @Override
        public boolean onNavigationItemSelected(MenuItem menuItem) {
            //Checking if the item is in checked state or not, if not make it in checked state
            if (menuItem.isChecked()) menuItem.setChecked(false);
            else menuItem.setChecked(true);

            //Closing drawer on item click
            mDrawerLayout.closeDrawers();

            //Check to see which item was being clicked and perform appropriate action
            switch (menuItem.getItemId()) {
                case R.id.home:
                    setToolBarTitle(R.string.nav_home);
                    replaceFragment(HomeFragment.getInstance());
                    return true;

                case R.id.liked:
                    setToolBarTitle(R.string.nav_liked);
                    replaceFragment(LikedFragment.getInstance());
                    //liked
                    return true;
                case R.id.self_promotion:
                    setToolBarTitle(R.string.nav_self_promote);
                    replaceFragment(SelfPromotionFragment.getInstance());
                    //shameless self plug
                    return true;
                case R.id.exit:
                    closeApplication();
                    return true;
                default:
                    replaceFragment(HomeFragment.getInstance());
                    setToolBarTitle(R.string.nav_home);
                    return true;
            }
        }
    }
}
