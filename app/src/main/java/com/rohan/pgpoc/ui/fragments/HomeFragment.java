package com.rohan.pgpoc.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.rohan.pgpoc.R;
import com.rohan.pgpoc.communication.bus.BusProvider;
import com.rohan.pgpoc.communication.manager.events.FetchedApiHitsEvent;
import com.rohan.pgpoc.communication.manager.events.FetchedGatewayListEvent;
import com.rohan.pgpoc.communication.manager.events.GetApiHitsEvent;
import com.rohan.pgpoc.communication.manager.events.GetPaymentGatewayListEvent;
import com.rohan.pgpoc.models.PaymentGateway;
import com.rohan.pgpoc.ui.adapters.PaymentGatewayAdaptor;
import com.rohan.pgpoc.ui.utils.MiscUtils;
import com.rohan.pgpoc.utils.FontUtils;
import com.rohan.pgpoc.utils.SortUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * Created by rohan on 11/3/16.
 */
public class HomeFragment extends Fragment {
    private static HomeFragment instance;
    private View view;

    private Bus mBus = BusProvider.getInstance();
    private RecyclerView rvPayemntGateways;
    private ProgressBar progressBar;
    private List<PaymentGateway> paymentGateways = new ArrayList<>();
    private PaymentGatewayAdaptor mAdapter;
    private TextView tvGatewaysCount;
    private TextView tvApiCount;
    private SegmentedGroup sgSorter;
    private List<PaymentGateway> sortedList = new ArrayList<>();
    private EditText etSearch;

    public HomeFragment() {
    }

    public static HomeFragment getInstance() {
        if (instance == null)
            instance = new HomeFragment();
        else instance.clearData();

        return instance;
    }

    private void clearData() {

    }

    @Override
    public void onResume() {
        super.onResume();
        mBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mBus.unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        FontUtils.init(view);
        initViews();
        initApiCalls();
        return view;
    }

    private void initApiCalls() {
        initApiForGatewayList();
        initApiForHitCounter();
    }

    private void initViews() {
        initRecyclerView();
        initCounters();
        initSorterSegways();
        initSearchFilter();
    }

    private void initSearchFilter() {
        etSearch = (EditText) view.findViewById(R.id.et_rv_filter);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initSorterSegways() {
        sgSorter = (SegmentedGroup) view.findViewById(R.id.sg_sorter);

        //sorting logic inside
        sgSorter.getChildAt(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reInit();
                sortedList = SortUtils.sortByCost(sortedList);
                mAdapter.swap(sortedList);
            }
        });
        sgSorter.getChildAt(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reInit();
                sortedList = SortUtils.sortByRating(sortedList);
                mAdapter.swap(sortedList);
            }
        });
        sgSorter.getChildAt(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reInit();
                sortedList = SortUtils.sortByName(sortedList);
                mAdapter.swap(sortedList);
            }
        });
        sgSorter.getChildAt(3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.swap(paymentGateways);
            }
        });
    }

    private void reInit() {
        if(paymentGateways.isEmpty()){
            return;
        }
        sortedList.clear();
        sortedList.addAll(paymentGateways);
    }

    private void initCounters() {
        tvApiCount = (TextView) view.findViewById(R.id.tv_api_count);
        tvGatewaysCount = (TextView) view.findViewById(R.id.tv_gateways_count);
    }

    private void initRecyclerView() {
        rvPayemntGateways = (RecyclerView) view.findViewById(R.id.rv_payment_gateways);
        rvPayemntGateways.setLayoutManager(new LinearLayoutManager(getActivity()));

        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        mAdapter = new PaymentGatewayAdaptor(getActivity(), paymentGateways);
//        rvPayemntGateways.addItemDecoration(new DividerItemDecoration(getActivity(), null));
        rvPayemntGateways.setAdapter(mAdapter);
    }

    private void notifyDataChange(List<PaymentGateway> paymentGateways) {
        mAdapter.swap(paymentGateways);
    }

    public void setHitsCounter(String hitsCounter) {
        tvApiCount.setText(hitsCounter);
    }

    public void setGatewaysCount(int size) {
        tvGatewaysCount.setText(""+size);
    }
    /**
     * api hit
     */
    private void initApiForGatewayList() {
        if (MiscUtils.isConnectedToInternet(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            mBus.post(new GetPaymentGatewayListEvent());
        } else {
            MiscUtils.showToast(getActivity(), getString(R.string.error_nointernet));
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * api consumed through framework logic
     */
    @Subscribe
    public void onFetchedPaymentGatewayListEvent(FetchedGatewayListEvent event) {
        if (!event.isStatus()) {
            MiscUtils.showToast(getActivity(), event.getErrorMessage());
        } else {
            paymentGateways = event.getData();
            if (paymentGateways.isEmpty()) {
                MiscUtils.showToast(getActivity(), getString(R.string.error_no_data_returned));
            }else{
                notifyDataChange(paymentGateways);
                setGatewaysCount(paymentGateways.size());
                etSearch.setEnabled(true);
            }
        }
        progressBar.setVisibility(View.GONE);
    }

    /**
     * api for hits
     */
    private void initApiForHitCounter() {
        if (MiscUtils.isConnectedToInternet(getActivity())) {
            mBus.post(new GetApiHitsEvent());
        } else {}
    }

    /**
     * consumed api for hits
     */
    @Subscribe
    public void onFetchedApiHitsCounter(FetchedApiHitsEvent event) {
        if (!event.isStatus()) {
            MiscUtils.showToast(getActivity(), event.getErrorMessage());
        } else {
            setHitsCounter(event.getData());
        }
    }

}
