package com.rohan.pgpoc.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rohan.pgpoc.R;
import com.rohan.pgpoc.models.PaymentGateway;
import com.rohan.pgpoc.models.dao.PaymentGatewayDao;
import com.rohan.pgpoc.ui.adapters.PaymentGatewayAdaptor;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by rohan on 12/3/16.
 */
public class LikedFragment extends Fragment{
    private static LikedFragment instance;
    private View view;
    private List<PaymentGateway> paymentGateways;
    private RecyclerView rvPayemntGateways;
    private PaymentGatewayAdaptor mAdapter;

    public LikedFragment() {
    }

    public static LikedFragment getInstance() {
        if (instance == null)
            instance = new LikedFragment();
        else instance.clearData();

        return instance;
    }

    private void clearData() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_liked, container, false);

        initViews();
        return view;
    }

    private void initViews() {
        initData();
        initRecylerView();
    }

    private void initData() {
        Realm realm = Realm.getInstance(getActivity());
        RealmResults<PaymentGatewayDao> results =
                realm.where(PaymentGatewayDao.class).findAll();

        paymentGateways =new ArrayList<>();
        for(PaymentGatewayDao gatewayDao:results) {
            paymentGateways.add(getPojoFromDao(gatewayDao));
        }
    }



    private void initRecylerView() {
        rvPayemntGateways = (RecyclerView) view.findViewById(R.id.rv_liked_payment_gateways);
        rvPayemntGateways.setLayoutManager(new LinearLayoutManager(getActivity()));

        mAdapter = new PaymentGatewayAdaptor(getActivity(), paymentGateways);
//        rvPayemntGateways.addItemDecoration(new DividerItemDecoration(getActivity(), null));
        rvPayemntGateways.setAdapter(mAdapter);
    }

    private PaymentGateway getPojoFromDao(PaymentGatewayDao dao) {
        PaymentGateway pojo = new PaymentGateway();
        pojo.setId(dao.getId());
        pojo.setName(dao.getName());
        pojo.setBranding(dao.getBranding());
        pojo.setCurrencies(dao.getCurrencies());
        pojo.setDescription(dao.getDescription());
        pojo.setImage(dao.getImage());
        pojo.setInstructions(dao.getInstructions());
        pojo.setRating(dao.getRating());
        pojo.setSetupCost(dao.getSetupCost());
        pojo.setTransactionCost(dao.getTransactionCost());
        return pojo;
    }
}
