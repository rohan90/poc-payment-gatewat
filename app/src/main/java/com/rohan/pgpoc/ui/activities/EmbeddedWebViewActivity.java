package com.rohan.pgpoc.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.rohan.pgpoc.R;
import com.rohan.pgpoc.definitions.Constants;
import com.rohan.pgpoc.utils.FontUtils;

/**
 * Created by rohan on 12/3/16.
 */
public class EmbeddedWebViewActivity extends BaseActivity {

    private TextView tvTitle;
    private TextView tvBack;
    private TextView tvNext;

    private WebView webView;
    private String url;

    protected void onCreate(Bundle savedInstanceState) {//doubt why here initviews
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_embedded_webview);

        FontUtils.init(this);
        initViews();
    }

    private void initViews() {
        initToolbar();
        initData();
        initWebView();

    }

    private void initToolbar() {
        tvTitle = (TextView) findViewById(R.id.tv_appbar_title_whensignedin);
        tvTitle.setText("...");

        tvBack = (TextView) findViewById(R.id.tv_appbar_left_whensignedin);
        tvBack.setText(getString(R.string.back));

        tvNext = (TextView) findViewById(R.id.tv_appbar_right_whensignedin);
        tvNext.setVisibility(TextView.GONE);

        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void initData() {
        url = getIntent().getStringExtra(Constants.BUNDLE_KEYS.WEBVIEW_URL);
    }

    private void initWebView() {
        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());
        openURL();
    }

    /** Opens the URL in a browser */
    private void openURL() {
        toast(getString(R.string.please_wait));
        webView.loadUrl("https://docs.google.com/gview?embedded=true&url="+url);
        webView.requestFocus();
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
