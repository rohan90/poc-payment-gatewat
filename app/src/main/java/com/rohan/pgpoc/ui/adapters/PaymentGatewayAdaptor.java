package com.rohan.pgpoc.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rohan.pgpoc.R;
import com.rohan.pgpoc.definitions.Constants;
import com.rohan.pgpoc.models.PaymentGateway;
import com.rohan.pgpoc.ui.activities.EmbeddedWebViewActivity;
import com.rohan.pgpoc.ui.activities.HomeActivity;
import com.rohan.pgpoc.ui.activities.PaymentGatewayPreviewActivity;
import com.rohan.pgpoc.ui.utils.MiscUtils;
import com.rohan.pgpoc.utils.FontUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by rohan on 12/3/16.
 */
public class PaymentGatewayAdaptor extends RecyclerView.Adapter<PaymentGatewayAdaptor.PaymentGatewayViewHolder> implements Filterable{

    private List<PaymentGateway> mPaymentGateways;
    private List<PaymentGateway> mDataSetBackup = new ArrayList<>();
    private List<PaymentGateway> filteredList;
    private Context mContext;

    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            PaymentGatewayViewHolder holder = (PaymentGatewayViewHolder) view.getTag();
            int position = holder.getAdapterPosition();

            showDetails(mPaymentGateways.get(position));
        }
    };

    public PaymentGatewayAdaptor(Context context, List<PaymentGateway> paymentGateways) {
        this.mContext = context;
        this.mPaymentGateways = paymentGateways;
        this.filteredList = new ArrayList<>();
    }

    @Override
    public PaymentGatewayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_payment_gateway, null);

        FontUtils.init(view);

        PaymentGatewayViewHolder viewHolder = new PaymentGatewayViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PaymentGatewayViewHolder holder, int position) {
        final PaymentGateway paymentGateway = mPaymentGateways.get(position);

        holder.tvTitle.setText(paymentGateway.getName());
        holder.llContainer.setOnClickListener(mClickListener);
        holder.llContainer.setTag(holder);

        holder.ivDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWebView(paymentGateway.getInstructions());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPaymentGateways.size();
    }

    public void swap(List<PaymentGateway> data) {
        mPaymentGateways.clear();
        mPaymentGateways.addAll(data);
        notifyDataSetChanged();

        mDataSetBackup.addAll(data);
    }

    private void showWebView(String url) {
        Intent intent = new Intent(((FragmentActivity) mContext),EmbeddedWebViewActivity.class);
        intent.putExtra(Constants.BUNDLE_KEYS.WEBVIEW_URL,url);
        mContext.startActivity(intent);
    }

    private void showDetails(PaymentGateway paymentGateway) {
        Intent intent = new Intent(((FragmentActivity) mContext),PaymentGatewayPreviewActivity.class);
        intent.putExtra(Constants.BUNDLE_KEYS.PAYMENT_GATEWAY,paymentGateway);
        mContext.startActivity(intent);
    }

    @Override
    public Filter getFilter() {
        return new CustomFilter(this,mPaymentGateways);
    }

    public class PaymentGatewayViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llContainer;
        TextView tvTitle;
        ImageView ivDownload;

        public PaymentGatewayViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            ivDownload = (ImageView) itemView.findViewById(R.id.iv_download);
            llContainer = (LinearLayout) itemView.findViewById(R.id.container_ll_row_payment_gateway);
        }
    }

    private class CustomFilter extends Filter {
        private final PaymentGatewayAdaptor adapter;
        private final List<PaymentGateway> originalList;
        private final List<PaymentGateway> filteredList;

        public CustomFilter(PaymentGatewayAdaptor paymentGatewayAdaptor, List<PaymentGateway> mPaymentGateways) {
            super();
            this.adapter = paymentGatewayAdaptor;
            this.originalList = new LinkedList<>(mPaymentGateways);
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0 || constraint.toString().equals("")) {
                filteredList.addAll(mDataSetBackup);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (final PaymentGateway gateway : originalList) {
                    boolean hasName = gateway.getName().toLowerCase().contains(filterPattern);
                    boolean hasCurrency = gateway.getCurrencies().toLowerCase().contains(filterPattern);
                    if ( hasName || hasCurrency) {
                        filteredList.add(gateway);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filteredList.clear();
            adapter.filteredList.addAll((ArrayList<PaymentGateway>) results.values);
            adapter.setFilteredDataIntoView();
            adapter.notifyDataSetChanged();
        }
    }

    private void setFilteredDataIntoView() {
            mPaymentGateways.clear();
            mPaymentGateways.addAll(filteredList);
    }
}
