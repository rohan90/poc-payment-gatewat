package com.rohan.pgpoc.communication.api;

import com.google.gson.Gson;
import com.rohan.pgpoc.definitions.Constants;
import com.rohan.pgpoc.models.responseDTOs.GetApiHitsResponseDTO;
import com.rohan.pgpoc.models.responseDTOs.GetPaymentGatewayListResponseDTO;
import com.rohan.pgpoc.utils.GsonUtils;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by rohan on 11/3/16.
 */
public class RestClient implements IPaymentGateways{
    private static final String URL = Constants.BASE_URL;

    private static RestClient mRestClient;
    private static RestAdapter mRestAdapter;

    public static RestClient getClient(){
        if(mRestClient == null)
            mRestClient = new RestClient();
        return mRestClient;
    }

    Gson gson = GsonUtils.getDateCompatibleGson();

    private RestClient(){
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS);

        mRestAdapter = new RestAdapter.Builder()
                .setEndpoint(URL)
                .setClient(new OkClient(okHttpClient))
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
    }

    /**
     * linking of interface to rest-client
     */

    @Override
    public void getPaymentGateways(Callback<GetPaymentGatewayListResponseDTO> callback) {
        IPaymentGateways gateways = mRestAdapter.create(IPaymentGateways.class);
        gateways.getPaymentGateways(callback);
    }

    @Override
    public void getApiHits(Callback<GetApiHitsResponseDTO> callback) {
        IPaymentGateways gateways = mRestAdapter.create(IPaymentGateways.class);
        gateways.getApiHits(callback);
    }
}
