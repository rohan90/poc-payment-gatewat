package com.rohan.pgpoc.communication.manager;

import android.content.Context;

import com.rohan.pgpoc.applications.App;
import com.rohan.pgpoc.communication.api.RestClient;
import com.rohan.pgpoc.communication.manager.events.FetchedApiHitsEvent;
import com.rohan.pgpoc.communication.manager.events.FetchedGatewayListEvent;
import com.rohan.pgpoc.communication.manager.events.GetApiHitsEvent;
import com.rohan.pgpoc.communication.manager.events.GetPaymentGatewayListEvent;
import com.rohan.pgpoc.models.responseDTOs.GetApiHitsResponseDTO;
import com.rohan.pgpoc.models.responseDTOs.GetPaymentGatewayListResponseDTO;
import com.rohan.pgpoc.utils.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by rohan on 11/3/16.
 */
public class EventManager {
    private Context mContext;
    private Bus mBus;
    private RestClient mRestClient;

    public EventManager(Context mContext, Bus mBus) {
        this.mContext = mContext;
        this.mBus = mBus;
        this.mRestClient = RestClient.getClient();
    }

    /**
     * listening to events
     */

    @Subscribe
    public void onGetPaymentGatewayListEvent(GetPaymentGatewayListEvent event) {
        Callback<GetPaymentGatewayListResponseDTO> callback = new Callback<GetPaymentGatewayListResponseDTO>() {

            @Override
            public void success(GetPaymentGatewayListResponseDTO restResponse, Response response) {
                Logger.logInfo("response:" + response.toString());
                Logger.logInfo("list of pgs:" + restResponse.toString());

                boolean status = true;
                String errorMessage = null;

                mBus.post(new FetchedGatewayListEvent(restResponse.getData(), status, errorMessage));
            }

            @Override
            public void failure(RetrofitError error) {
                boolean status = false;
                String errorMessage = error.getLocalizedMessage();

                mBus.post(new FetchedGatewayListEvent(null, false, errorMessage));
            }
        };
        mRestClient.getPaymentGateways(callback);
    }

    @Subscribe
    public void onGetApiHitsEvent(GetApiHitsEvent event) {
        Callback<GetApiHitsResponseDTO> callback = new Callback<GetApiHitsResponseDTO>() {

            @Override
            public void success(GetApiHitsResponseDTO restResponse, Response response) {
                Logger.logInfo("response:" + response.toString());
                Logger.logInfo("api_hits:" + restResponse.toString());

                boolean status = true;
                String errorMessage = null;

                mBus.post(new FetchedApiHitsEvent(restResponse.getData(), status, errorMessage));
            }

            @Override
            public void failure(RetrofitError error) {
                boolean status = false;
                String errorMessage = error.getLocalizedMessage();

                mBus.post(new FetchedApiHitsEvent(null, false, errorMessage));
            }
        };
        mRestClient.getApiHits(callback);
    }
}
