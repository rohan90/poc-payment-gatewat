package com.rohan.pgpoc.communication.manager.events;

/**
 * Created by rohan on 12/3/16.
 */
public class FetchedApiHitsEvent {
    private String data;
    private String errorMessage;
    private boolean status;

    public FetchedApiHitsEvent(String data, boolean status, String errorMessage) {
        this.data = data;
        this.status = status;
        this.errorMessage = errorMessage;
    }

    public String getData() {
        return data;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public boolean isStatus() {
        return status;
    }
}
