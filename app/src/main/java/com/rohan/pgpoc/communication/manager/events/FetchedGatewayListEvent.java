package com.rohan.pgpoc.communication.manager.events;

import com.google.gson.annotations.SerializedName;
import com.rohan.pgpoc.models.PaymentGateway;

import java.util.List;

/**
 * Created by rohan on 12/3/16.
 */
public class FetchedGatewayListEvent {
    private List<PaymentGateway> data;
    private String errorMessage;
    private boolean status;

    public FetchedGatewayListEvent(List<PaymentGateway> data, boolean status, String errorMessage) {
        this.data = data;
        this.status = status;
        this.errorMessage = errorMessage;
    }

    public List<PaymentGateway> getData() {
        return data;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public boolean isStatus() {
        return status;
    }
}
