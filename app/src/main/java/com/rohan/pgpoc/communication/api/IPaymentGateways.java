package com.rohan.pgpoc.communication.api;

import com.rohan.pgpoc.definitions.Constants;
import com.rohan.pgpoc.models.responseDTOs.GetApiHitsResponseDTO;
import com.rohan.pgpoc.models.responseDTOs.GetPaymentGatewayListResponseDTO;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by rohan on 12/3/16.
 */
public interface IPaymentGateways {

    @GET(Constants.REST_API.GET_LIST_OF_GATEWAYS)
    void getPaymentGateways(Callback<GetPaymentGatewayListResponseDTO> callback);

    @GET(Constants.REST_API.GET_API_HITS)
    void getApiHits(Callback<GetApiHitsResponseDTO> callback);
}
